package com.seren.todomvctest.brn1;

import com.seren.todomvctest.brn1.features.TodosE2ETest;
import com.seren.todomvctest.brn1.features.TodosOperationsAtAllFilterTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses({TodosE2ETest.class, TodosOperationsAtAllFilterTest.class})
public class AllSuiteTest {
}
