package com.seren.todomvctest.brn1;

import com.seren.todomvctest.brn1.categories.Buggy;
import com.seren.todomvctest.brn1.features.TodosOperationsAtAllFilterTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Suite.SuiteClasses({TodosOperationsAtAllFilterTest.class, TodosOperationsAtAllFilterTest.class})
@Categories.IncludeCategory(Buggy.class)
public class BuggySuiteTest {
}
