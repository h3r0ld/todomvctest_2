package com.seren.todomvctest.brn1.features;

import com.seren.todomvctest.brn1.categories.Smoke;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.seren.todomvctest.brn1.helpers.GivenHelpers.givenAtAll;
import static com.seren.todomvctest.brn1.pages.TodoMVC.*;
import static com.seren.todomvctest.brn1.pages.TodoMVC.assertNoTasks;
import static com.seren.todomvctest.brn1.pages.TodoMVC.delete;


/**
 * Created by Herold on 13.07.2016.
 */
public class TodosE2ETest extends BaseTest {

    @Test
    @Category(Smoke.class)
    public void testTasksCommonFlow(){
        givenAtAll();
        add("A");
        edit("A" , "A_edited");

        //complete
        toggle("A_edited");
        assertTasks("A_edited");

        filterActive();
        assertNoVisibleTasks();

        add("B");
        assertVisibleTasks("B");
        assertItemsLeft(1);

        //complete all
        toggleAll();
        assertNoVisibleTasks();

        filterCompleted();
        assertVisibleTasks("A_edited","B");

        //reopen
        toggle("A_edited");
        assertVisibleTasks("B");

        clearCompleted();
        assertNoVisibleTasks();

        filterAll();
        assertTasks("A_edited");

        delete("A_edited");
        assertNoTasks();
    }
}
