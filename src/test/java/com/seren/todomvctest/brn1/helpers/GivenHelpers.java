package com.seren.todomvctest.brn1.helpers;

import com.codeborne.selenide.WebDriverRunner;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.enabled;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

public class GivenHelpers {
    @Step
    public static void givenAtCompleted(Task... tasks){
        given(Filter.COMPLETED, tasks);
    }

    @Step
    public static void givenAtCompleted(TaskType tasktype, String... tasksTexts){
        given(Filter.COMPLETED,taskArray(tasktype,tasksTexts));
    }

    @Step
    public static void givenAtActive(Task... tasks){
        given(Filter.ACTIVE, tasks);
    }

    @Step
    public static void givenAtActive(TaskType tasktype, String... tasksTexts){
        given(Filter.ACTIVE,taskArray(tasktype,tasksTexts));
    }

    @Step
    public static void givenAtAll(Task... tasks){
        given(Filter.ALL, tasks);
    }

    @Step
    public static void givenAtAll(TaskType type, String... tasksTexts){
        given(Filter.ALL, taskArray(type, tasksTexts));
    }

    @Step
    private static void given(Filter filter, String... tasksTexts){
        given(filter, taskArray(TaskType.ACTIVE, tasksTexts));
    }

    @Step
    private static void given(Filter filter, Task... tasks){
        ensureUrlIsOpened(filter);
        prepareTasks(tasks);
        executeJavaScript("location.reload()");
        ensurePageIsLoaded();
    }

    @Step
    public static void ensurePageIsLoaded(){
        $("#new-todo").shouldBe(enabled);
    }

    @Step
    public static void ensureUrlIsOpened(Filter filter){

        if (!(WebDriverRunner.url().equals(filter.url())))
            open(filter.url());

        ensurePageIsLoaded();
    }


    public static void prepareTasks(Task... tasks){
        StringBuilder tasksString = new StringBuilder();

        tasksString.append("localStorage.setItem('todos-troopjs', \"[");
        for (Task task: tasks){
            tasksString.append("{\\\"completed\\\":").append((task.taskType == TaskType.COMPLETED)? "true,": "false,");
            tasksString.append(" \\\"title\\\":\\\""+task.taskText +"\\\"},");
        }
        if(tasks.length != 0)
            tasksString.deleteCharAt(tasksString.lastIndexOf(","));

        tasksString.append("]\")");
        executeJavaScript(tasksString.toString());
    }

    public static Task[] taskArray(TaskType type, String... tasksTexts){
        Task[] tasksArray = new Task[tasksTexts.length];
        for (int i = 0; i < tasksTexts.length; i++){
            tasksArray[i] = new Task(tasksTexts[i] ,type);
        }
        return tasksArray;
    }

    public static class Task {
        String taskText;
        TaskType taskType;

        public Task(String taskText, TaskType taskType) {
            this.taskText = taskText;
            this.taskType = taskType;
        }
    }

    public enum TaskType {
        ACTIVE,
        COMPLETED;
    }

    public enum Filter {
        ALL(""),
        ACTIVE("/#/active"),
        COMPLETED("/#/completed");

        String subUrl;


        Filter(String subUrl){
            this.subUrl = subUrl;
        }

        public String url(){
            return "https://todomvc4tasj.herokuapp.com"+subUrl;
        }
    }
}
